package kz.turaqshare.nurcelona.www.turaqshare.activities.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;

public class OptionActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);
    }
    @OnClick(R.id.offer_parking)
    void onParkingOffer(){
        startActivity(new Intent(this, ParkingInfoActivity.class));
    }
}
