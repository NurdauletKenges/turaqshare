package kz.turaqshare.nurcelona.www.turaqshare.activities.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.maps.android.clustering.ClusterManager;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.HistoryFragment;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.LogOutFragmentDialog;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.ParkingFragment;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.PaymentFragment;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.TarifsFragment;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.MyItem;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.Parking;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.User;
import timber.log.Timber;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    SupportMapFragment mapFragment;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.location)
    EditText location;
    @BindView(R.id.app_bar)
    View app_bar;
    @BindView(R.id.bottom_sheet)
    LinearLayout llBottomSheet;
    @BindView(R.id.version)
    TextView version;
    private GoogleMap mMap;
    private BottomSheetBehavior bottomSheetBehavior;
    private TextView tv_name;
    private TextView tv_phone;
    private ImageView imageView;
    private FirebaseAuth mAuth;
    private Marker marker;
    private ProgressDialog progressDialog;
    private NavigationView navigationView;
    private Context mContext;
    private int total_num = 0;
    private int taken = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = MainActivity.this;

        initNavigationDrawer();
        initMap();
        initFirebase();
        initInfo();
        initBottomSheet();
        initProgressDialog();
        updateProfilePhoto();
        loadData();

        String merchant = "<merchant cert_id=\"00c183d70b\" name=\"New Demo Shop\"><order order_id=\"1204170829\" amount=\"50\" currency=\"398\"><department merchant_id=\"92061103\" amount=\"50\"/></order></merchant>";

        merchant = new StringBuilder(merchant).reverse().toString();
        byte[] data = merchant.getBytes();
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        Log.e("MainActivity", "base64: " + base64);

    }


    private void updateProfilePhoto() {
        FirebaseStorage.getInstance()
                .getReference()
                .child("profileImages")
                .child(mAuth.getCurrentUser().getUid())
                .child("profile")
                .getDownloadUrl().addOnSuccessListener(uri ->
                Glide.with(this).load(uri)
                        .thumbnail(Glide.with(this).load(R.drawable.placeholder))
                        .into(imageView));
        //Glide.with(MainActivity.this).load(uri).into(imageView).onLoadStarted());
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Загрузка");
    }

    private void initBottomSheet() {
        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    private void initNavigationDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        View headerView = navigationView.getHeaderView(0);
        tv_name = headerView.findViewById(R.id.tv_name);
        tv_phone = headerView.findViewById(R.id.tv_phone);
        imageView = headerView.findViewById(R.id.imageView);
        headerView.findViewById(R.id.edit_profile).setOnClickListener(view -> openEditProfile());
        imageView.setOnClickListener(view -> startActivityForResult(new Intent(MainActivity.this, EditProfileActivity.class), 71));
    }

    private void openEditProfile() {
        startActivityForResult(new Intent(MainActivity.this, EditProfileActivity.class), 71);
    }

    private void initMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initFirebase() {
        mAuth = FirebaseAuth.getInstance();
    }

    private void initInfo() {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("users").child(mAuth.getCurrentUser().getUid());
        ValueEventListener infoListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null && tv_phone != null && tv_name != null) {
                    tv_name.setText(user.getName());
                    tv_phone.setText(user.getPhone());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Timber.e(databaseError.toException());
                // ...
            }
        };
        myRef.addValueEventListener(infoListener);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version.setText(getString(R.string.version, pInfo.versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setLocation(Place place) {
        if (mMap != null) {
            mMap.clear();
            setMarkers();
            mMap.addMarker(new MarkerOptions().position(place.getLatLng()));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
            location.setText(place.getName());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
            mMap.moveCamera(zoom);
        }
    }

    @OnClick(R.id.burger)
    void OnBurgerClick() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @OnClick(R.id.location)
    void onLocationClick() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setBoundsBias(new LatLngBounds(new LatLng(51.002009, 71.174569),
                                    new LatLng(51.307506, 71.743749)))
                            .build(this);
            startActivityForResult(intent, 69);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (container.getVisibility() == View.VISIBLE) {
                setCurrentFragmentToMain();
                return;
            }
            super.onBackPressed();
            finishAffinity();
        }
    }

    private void setCurrentFragmentToMain() {
        if (mapFragment.getView() != null)
            mapFragment.getView().setVisibility(View.VISIBLE);
        container.setVisibility(View.GONE);
        title.setText(getText(R.string.choose_location));
        title.setVisibility(View.VISIBLE);
        location.setVisibility(View.VISIBLE);
        app_bar.setVisibility(View.VISIBLE);
        navigationView.setCheckedItem(R.id.main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        llBottomSheet.setVisibility(View.GONE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment;
        switch (id) {
            case R.id.main:
                setCurrentFragmentToMain();
                break;
            case R.id.rent:
                if (mapFragment.getView() != null)
                    mapFragment.getView().setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                fragment = new ParkingFragment();
                fragmentTransaction.replace(R.id.container, fragment);
                //app_bar.setVisibility(View.GONE);
                title.setVisibility(View.VISIBLE);
                title.setText(R.string.my_parking);
                location.setVisibility(View.GONE);
                break;
            case R.id.history:
                if (mapFragment.getView() != null)
                    mapFragment.getView().setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                fragment = new HistoryFragment();
                fragmentTransaction.replace(R.id.container, fragment);
                title.setText(R.string.history);
                title.setVisibility(View.VISIBLE);
                location.setVisibility(View.GONE);
                app_bar.setVisibility(View.VISIBLE);
                break;
            case R.id.payment:
                if (mapFragment.getView() != null)
                    mapFragment.getView().setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                fragment = new PaymentFragment();
                fragmentTransaction.replace(R.id.container, fragment);
                title.setVisibility(View.VISIBLE);
                title.setText(R.string.payment);
                location.setVisibility(View.GONE);
                app_bar.setVisibility(View.VISIBLE);
                break;
            case R.id.tarifs:
                if (mapFragment.getView() != null)
                    mapFragment.getView().setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
                fragment = new TarifsFragment();
                fragmentTransaction.replace(R.id.container, fragment);
                title.setVisibility(View.VISIBLE);
                title.setText(R.string.tarifs);
                location.setVisibility(View.GONE);
                app_bar.setVisibility(View.VISIBLE);
                break;
            case R.id.support:
                startActivity(new Intent(MainActivity.this, HelpActivity.class));
                setCurrentFragmentToMain();
        }
        fragmentTransaction.commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng astana = new LatLng(51.13014, 71.427156);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(astana, 13);
        mMap.moveCamera(cameraUpdate);

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(mContext);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(mContext);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(mContext);
                snippet.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
                title.setGravity(Gravity.CENTER);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });

        ClusterManager<MyItem> mClusterManager = new ClusterManager<>(this, mMap);
        mMap.setOnCameraIdleListener(mClusterManager);
        setMarkers();
    }

    private void setMarkers() {
        //mClusterManager.clearItems();
        mMap.clear();
        LatLng point1 = new LatLng(51.130185, 71.436364);
        LatLng point2 = new LatLng(51.126448, 71.434264);
        LatLng point3 = new LatLng(51.13014, 71.427156);
        LatLng point4 = new LatLng(51.128913, 71.423578);
        LatLng point5 = new LatLng(51.126549, 71.422484);
        LatLng point6 = new LatLng(51.114553, 71.431185);
        LatLng point7 = new LatLng(51.119849, 71.439575);
        LatLng point8 = new LatLng(51.138776, 71.417516);
        LatLng point9 = new LatLng(51.142202, 71.417109);
        LatLng point10 = new LatLng(51.115188, 71.427954);
        LatLng point11 = new LatLng(51.145931, 71.424233);
        LatLng point12 = new LatLng(51.113852, 71.438861);
        LatLng point13 = new LatLng(51.135942, 71.422636);
        LatLng point14 = new LatLng(51.132064, 71.437762);
        LatLng point15 = new LatLng(51.114339, 71.432717);
        LatLng point16 = new LatLng(51.116404, 71.433856);
        LatLng point17 = new LatLng(51.118233, 71.456709);
        LatLng point18 = new LatLng(51.118388, 71.455421);
        LatLng point19 = new LatLng(51.113762, 71.421797);
        LatLng point20 = new LatLng(51.121085, 71.476501);
        LatLng point21 = new LatLng(51.132528, 71.434988);
        LatLng point22 = new LatLng(51.132606, 71.436163);
        LatLng point23 = new LatLng(51.13207, 71.437756);
        LatLng point24 = new LatLng(51.118531, 71.420541);
        LatLng point25 = new LatLng(51.144895, 71.416312);
        LatLng point26 = new LatLng(51.121056, 71.414445);
        LatLng point27 = new LatLng(51.11896, 71.414571);
        LatLng point28 = new LatLng(51.11624, 71.433379);
        LatLng point29 = new LatLng(51.110201, 71.42591);
        LatLng point30 = new LatLng(51.159413, 71.407857);
        LatLng point31 = new LatLng(51.168204, 71.446044);
        LatLng point32 = new LatLng(51.16005, 71.409631);
        LatLng point33 = new LatLng(51.159821, 71.404352);
        LatLng point34 = new LatLng(51.162324, 71.407485);
        LatLng point35 = new LatLng(51.1634, 71.406949);
        LatLng point36 = new LatLng(51.16153, 71.401477);
        LatLng point37 = new LatLng(51.12641, 71.490366);
        LatLng point38 = new LatLng(51.174675, 71.404223);
        LatLng point39 = new LatLng(51.173061, 71.40461);
        LatLng point40 = new LatLng(51.160329, 71.414584);
        LatLng point41 = new LatLng(51.161261, 71.408279);
        LatLng point42 = new LatLng(51.159807, 71.414115);
        LatLng point43 = new LatLng(51.167163, 71.407885);
        LatLng point44 = new LatLng(51.166804, 71.406841);
        LatLng point45 = new LatLng(51.16811, 71.40461);
        LatLng point46 = new LatLng(51.167787, 71.38989);
        LatLng point47 = new LatLng(51.11434, 71.432076);
        LatLng point48 = new LatLng(51.116778, 71.430809);
        LatLng point49 = new LatLng(51.152429, 71.485522);
        LatLng point50 = new LatLng(51.131545, 71.496474);
        LatLng point51 = new LatLng(51.140236, 71.479819);
        LatLng point52 = new LatLng(51.167943, 71.437458);
        LatLng point53 = new LatLng(51.127694, 71.488439);
        LatLng point54 = new LatLng(51.112355, 71.435059);
        LatLng point55 = new LatLng(51.175307, 71.402594);
        LatLng point56 = new LatLng(51.173432, 71.401688);
        LatLng point57 = new LatLng(51.170518, 71.395533);
        LatLng point58 = new LatLng(51.169791, 71.395147);
        LatLng point59 = new LatLng(51.171676, 71.404517);
        LatLng point60 = new LatLng(51.113855, 71.438212);


        mMap.addMarker(new MarkerOptions().position(point1).title("ЖК Новый Мир").snippet(getString(R.string.taken_and_total, taken, total_num)));
        mMap.addMarker(new MarkerOptions().position(point2).title("ЖК Нурсая 2, ул Достык 13"));
        mMap.addMarker(new MarkerOptions().position(point3).title("ЖК на Водно Зеленом Бульваре, ул Кунаева 12/1"));
        mMap.addMarker(new MarkerOptions().position(point4).title("ЖК Северное Сияние, ул Достык 5/1"));
        mMap.addMarker(new MarkerOptions().position(point5).title("ЖК Алматы, ул Достык 10"));
        mMap.addMarker(new MarkerOptions().position(point6).title("ЖК Ақжайық, пр Мәнгілік Ел 19/2"));
        mMap.addMarker(new MarkerOptions().position(point7).title("ЖК Ишим, ул Бокейхана 2"));
        mMap.addMarker(new MarkerOptions().position(point8).title("ЖК Триумф Астаны, ул Кабанбай батыра 11"));
        mMap.addMarker(new MarkerOptions().position(point9).title("ЖК Премьера, ул Кабанбай батыра 7"));
        mMap.addMarker(new MarkerOptions().position(point10).title("ЖК Олимп Палас 1, ул Туркестан 8"));
        mMap.addMarker(new MarkerOptions().position(point11).title("ЖК Каскад, ул Кабанбай батыра 8"));
        mMap.addMarker(new MarkerOptions().position(point12).title("ЖК Қыз жібек, ул Бокейхана 13"));
        mMap.addMarker(new MarkerOptions().position(point13).title("ЖК Лазурный берег, ул Сарайшық 5"));
        mMap.addMarker(new MarkerOptions().position(point14).title("ЖК Версаль, ул Сарайшық 40"));
        mMap.addMarker(new MarkerOptions().position(point15).title("ЖК Айсанам, пр Мәнгілік Ел 19"));
        mMap.addMarker(new MarkerOptions().position(point16).title("ЖК Көркем 1, пр Мәнгілік Ел 17"));
        mMap.addMarker(new MarkerOptions().position(point17).title("ЖК \"Итальянский квартал\""));
        mMap.addMarker(new MarkerOptions().position(point18).title("ЖК \"Французский квартал\""));
        mMap.addMarker(new MarkerOptions().position(point19).title("ЖК \"Expo towers\""));
        mMap.addMarker(new MarkerOptions().position(point20).title("ЖК \"Миллениум парк\""));
        mMap.addMarker(new MarkerOptions().position(point21).title("ЖК \"Арайлым\""));
        mMap.addMarker(new MarkerOptions().position(point22).title("ЖК \"Премиум\""));
        mMap.addMarker(new MarkerOptions().position(point23).title("ЖК \"Версаль-Есиль\""));
        mMap.addMarker(new MarkerOptions().position(point24).title("ЖК Градкомплекс-3"));
        mMap.addMarker(new MarkerOptions().position(point25).title("ЖК \"Радуга\""));
        mMap.addMarker(new MarkerOptions().position(point26).title("ЖК \"Инфинити 1\""));
        mMap.addMarker(new MarkerOptions().position(point27).title("ЖК \"Инфинити 2\""));
        mMap.addMarker(new MarkerOptions().position(point28).title("ЖК \"Көркем 1\""));
        mMap.addMarker(new MarkerOptions().position(point29).title("ЖК \"Көркем 2\""));
        mMap.addMarker(new MarkerOptions().position(point30).title("ЖК \"Жагалау\""));
        mMap.addMarker(new MarkerOptions().position(point31).title("ЖК \"Каминный\""));
        mMap.addMarker(new MarkerOptions().position(point32).title("ЖК \"Болашак\""));
        mMap.addMarker(new MarkerOptions().position(point33).title("ЖК \"Сана\""));
        mMap.addMarker(new MarkerOptions().position(point34).title("ЖК \"Монблан\""));
        mMap.addMarker(new MarkerOptions().position(point35).title("ЖК \"Кенесары\""));
        mMap.addMarker(new MarkerOptions().position(point36).title("ЖК \"Эмират\""));
        mMap.addMarker(new MarkerOptions().position(point37).title("ЖК \"Турсын Астана\""));
        mMap.addMarker(new MarkerOptions().position(point38).title("ЖК \"Свечки\""));
        mMap.addMarker(new MarkerOptions().position(point39).title("ЖК \"Запад\""));
        mMap.addMarker(new MarkerOptions().position(point40).title("ЖК \"Титаник\""));
        mMap.addMarker(new MarkerOptions().position(point41).title("ЖК \"Edel\""));
        mMap.addMarker(new MarkerOptions().position(point42).title("ЖК \"Арман\""));
        mMap.addMarker(new MarkerOptions().position(point43).title("ЖК \"Бурлин 1\""));
        mMap.addMarker(new MarkerOptions().position(point44).title("ЖК \"Бурлин 2\""));
        mMap.addMarker(new MarkerOptions().position(point45).title("ЖК \"Авангард\""));
        mMap.addMarker(new MarkerOptions().position(point46).title("ЖК \"Сапа\""));
        mMap.addMarker(new MarkerOptions().position(point47).title("ЖК \"Айсанам\""));
        mMap.addMarker(new MarkerOptions().position(point48).title("ЖК \"Отандастар\""));
        mMap.addMarker(new MarkerOptions().position(point49).title("ЖК \"Мирас\""));
        mMap.addMarker(new MarkerOptions().position(point50).title("ЖК \"Барыс\""));
        mMap.addMarker(new MarkerOptions().position(point51).title("ЖК \"Кахарман\""));
        mMap.addMarker(new MarkerOptions().position(point52).title("ЖК \"Сатты\""));
        mMap.addMarker(new MarkerOptions().position(point53).title("ЖК \"Сатты-7\""));
        mMap.addMarker(new MarkerOptions().position(point54).title("ЖК \"Каратау\""));
        mMap.addMarker(new MarkerOptions().position(point55).title("ЖК \"Батыр\""));
        mMap.addMarker(new MarkerOptions().position(point56).title("ЖК \"Гармония\""));
        mMap.addMarker(new MarkerOptions().position(point57).title("ЖК \"Целиноград 1\""));
        mMap.addMarker(new MarkerOptions().position(point58).title("ЖК \"Целиноград 2\""));
        mMap.addMarker(new MarkerOptions().position(point59).title("ЖК \"Европейский 1\""));
        mMap.addMarker(new MarkerOptions().position(point60).title("ЖК \"Кыз-Жибек\""));

        /*List<MyItem> items = new ArrayList<>();
        items.add(new MyItem(point1.latitude, point1.longitude, "ЖК Новый Мир", getString(R.string.taken_and_total, taken, total_num)));
        items.add(new MyItem(point2.latitude, point2.longitude, "ЖК Нурсая 2, ул Достык 13", null));
        items.add(new MyItem(point3.latitude, point3.longitude, "ЖК на Водно Зеленом Бульваре, ул Кунаева 12/1", null));
        items.add(new MyItem(point4.latitude, point4.longitude, "ЖК Северное Сияние, ул Достык 5/1", null));
        items.add(new MyItem(point5.latitude, point5.longitude, "ЖК Алматы, ул Достык 10", null));
        items.add(new MyItem(point6.latitude, point6.longitude, "ЖК Ақжайық, пр Мәнгілік Ел 19/2", null));
        items.add(new MyItem(point7.latitude, point7.longitude, "ЖК Ишим, ул Бокейхана 2", null));
        items.add(new MyItem(point8.latitude, point8.longitude, "ЖК Триумф Астаны, ул Кабанбай батыра 11", null));
        items.add(new MyItem(point9.latitude, point9.longitude, "ЖК Премьера, ул Кабанбай батыра 7", null));
        items.add(new MyItem(point10.latitude, point10.longitude, "ЖК Олимп Палас 1, ул Туркестан 8", null));
        items.add(new MyItem(point11.latitude, point11.longitude, "ЖК Каскад, ул Кабанбай батыра 8", null));
        items.add(new MyItem(point12.latitude, point12.longitude, "ЖК Қыз жібек, ул Бокейхана 13", null));
        items.add(new MyItem(point13.latitude, point13.longitude, "ЖК Лазурный берег, ул Сарайшық 5", null));
        items.add(new MyItem(point14.latitude, point14.longitude, "ЖК Версаль, ул Сарайшық 40", null));
        items.add(new MyItem(point15.latitude, point15.longitude, "ЖК Айсанам, пр Мәнгілік Ел 19", null));
        items.add(new MyItem(point16.latitude, point16.longitude, "ЖК Көркем 1, пр Мәнгілік Ел 17", null));
        mClusterManager.addItems(items);*/
    }

    private void loadData() {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference()
                .child("parkings")
                .child("ЖК Новый Мир");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                total_num = 0;
                taken = 0;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String uuid = snapshot.getKey();
                    if (uuid != null) {
                        for (DataSnapshot inner_shot : snapshot.getChildren()) {
                            Parking parking = inner_shot.getValue(Parking.class);
                            if (parking != null) {
                                total_num++;
                                if (parking.getStatus().equals("Taken")) {
                                    taken++;
                                }
                            }
                        }
                    }
                }
                setMarkers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @OnClick(R.id.exit)
    void onExit() {
        LogOutFragmentDialog.newInstance().show(getSupportFragmentManager(), null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 69) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                LatLngBounds bounds = new LatLngBounds(new LatLng(51.002009, 71.174569),
                        new LatLng(51.307506, 71.743749));
                if (bounds.contains(place.getLatLng()))
                    setLocation(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            }
        }
        if (requestCode == 70) {
            bottomSheetBehavior.setHideable(true);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
        if (requestCode == 71) {
            updateProfilePhoto();
        }
        progressDialog.dismiss();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        llBottomSheet.setVisibility(View.VISIBLE);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setHideable(false);
        this.marker = marker;
        return false;
    }

    @OnClick(R.id.bottom_sheet)
    void onReadyClick() {
        if (marker != null) {
            progressDialog.show();
            startActivityForResult(new Intent(this, HousingEstateActivity.class).putExtra("title", marker.getTitle()), 70);
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        progressDialog.show();
        startActivityForResult(new Intent(this, HousingEstateActivity.class).putExtra("title", marker.getTitle()), 70);
    }
}
