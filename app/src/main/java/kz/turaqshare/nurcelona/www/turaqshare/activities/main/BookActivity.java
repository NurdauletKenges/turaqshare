package kz.turaqshare.nurcelona.www.turaqshare.activities.main;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.payment.PaymentActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.EpayConstants;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.EpayLanguage;


public class BookActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.address)
    TextView mAddress;
    Bundle mExtras;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        mExtras = getIntent().getExtras();
        title.setText(mExtras.getString("title"));
        mAddress.setText(mExtras.getString("address"));
        progressDialog = new ProgressDialog(this);


    }

    @OnClick(R.id.pay_button)
    void onPayButtonClick() {
        progressDialog.show();
   /*     FirebaseAuth mAuth = FirebaseAuth.getInstance();
        DatabaseReference myRef = FirebaseDatabase.getInstance()
                .getReference("parkings")
                .child("ЖК Новый Мир")
                .child(mExtras.getString("uuid"))
                .child(mExtras.getString("number"));
        myRef.child("status").setValue("Taken").addOnSuccessListener(aVoid -> {
            myRef.child("taken_by").setValue(mAuth.getCurrentUser().getUid()).addOnSuccessListener(aVoid1 -> {
                progressDialog.dismiss();
                finish();
            });
        }).addOnFailureListener(e -> progressDialog.dismiss());*/
        //startActivity(new Intent(this, PaymentActivity.class));

        try {
            /*String order = "<document><merchant cert_id=\"00c183d70b\" name=\"New Demo Shop\"><order order_id=\"1119134528\" amount=\"50\" currency=\"398\"><department merchant_id=\"98110191\" amount=\"50\"/></order></merchant><merchant_sign type=\"RSA\">Mx0uAk3+B1Xfwh/SqW0wQ/R3FQ4R+wPFv3tzfpZYxigV7UkAQNmfftCAfNU1zqJuEv5otJSPbYG5USQxC1rrYS6UGvw4S5fCmbbqFMkgHcHlZ78XKBMNG6RJHJznnoO6keRd59+jcEoit51VqWyaeSj+1OEOH6fXClgd4qtC1Tk=</merchant_sign></document>";

            byte[] data = order.getBytes("UTF-8");

            String base64 = Base64.encodeToString(data, Base64.DEFAULT);*/

            String postLink = "http://your_post_link";

            String merchant_id = "98110191";

            String document = "<document>";

            String document_start = "<merchant cert_id=\"c183ed67\" name=\"TURAQSHARE\"><order order_id=\"1204165557\" amount=\"50\" currency=\"398\"><department merchant_id=\"98110191\" amount=\"50\"/></order></merchant>";

            String order = "<merchant_sign type=\"RSA\">Mx0uAk3+B1Xfwh/SqW0wQ/R3FQ4R+wPFv3tzfpZYxigV7UkAQNmfftCAfNU1zqJuEv5otJSPbYG5USQxC1rrYS6UGvw4S5fCmbbqFMkgHcHlZ78XKBMNG6RJHJznnoO6keRd59+jcEoit51VqWyaeSj+1OEOH6fXClgd4qtC1Tk=</merchant_sign></document>";

            byte[] data = order.getBytes("UTF-8");

            //String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            String base64 = "PGRvY3VtZW50PjxtZXJjaGFudCBjZXJ0X2lkPSJjMTgzZWQ2NyIgbmFtZT0iVFVSQVFTSEFSRSI+PG9yZGVyIG9yZGVyX2lkPSI0NTYxMjM0NSIgYW1vdW50PSIzMjIwIiBjdXJyZW5jeT0iMzk4Ij48ZGVwYXJ0bWVudCBtZXJjaGFudF9pZD0iOTgxMTAxOTEiIGFtb3VudD0iMzIyMCIvPjwvb3JkZXI+PC9tZXJjaGFudD48bWVyY2hhbnRfc2lnbiB0eXBlPSJSU0EiPnBoMmUxM2s1YytWYVYvWVdGZ1RpcURtcVNkbTlxY2cxNmRCbFFkODRRVWV3clJ2QytsOEJUdEZET01YRjFhR1lHejJOWitueTI3UWZ0ZW93dTc3V0t5eHJSUDVEL215Q2crakphTUZ0MmxlcU45ZjdjY090YnNaUWpHalZiUEZhbEdBb0Zpa3FMWkhuWEFzVUJIRTdNam4rRDdZaWd2b0M5TjFQbUJPY2d6cz08L21lcmNoYW50X3NpZ24+PC9kb2N1bWVudD4=";

            //String template = "your_template";

            Intent intent = new Intent(BookActivity.this, PaymentActivity.class);
            intent.putExtra(EpayConstants.EXTRA_TEST_MODE, false);
            intent.putExtra(EpayConstants.EXTRA_SIGNED_ORDER_BASE_64, base64);
            intent.putExtra(EpayConstants.EXTRA_POST_LINK, postLink);
            intent.putExtra(EpayConstants.EXTRA_LANGUAGE, EpayLanguage.RUSSIAN);
            //intent.putExtra(EpayConstants.EXTRA_TEMPLATE, template);

            startActivityForResult(intent, EpayConstants.EPAY_PAY_REQUEST);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        progressDialog.dismiss();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
