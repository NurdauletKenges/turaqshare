package kz.turaqshare.nurcelona.www.turaqshare.activities.objects;

public class User {
    private String name;
    private String phone;
    private String car_number;
    private String email;

    public User() {
        //For Firebase
    }


    public User(String name, String phone, String car_number, String email) {
        this.name = name;
        this.phone = phone;
        this.car_number = car_number;
        this.email = email;
    }

    public String getEmail() {
        if (email != null)
            return email;
        return "";
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getCar_number() {
        return car_number;
    }
}
