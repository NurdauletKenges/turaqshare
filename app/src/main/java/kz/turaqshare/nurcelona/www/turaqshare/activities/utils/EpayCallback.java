package kz.turaqshare.nurcelona.www.turaqshare.activities.utils;

/**
 * epay-sdk-android
 * Created by http://beemobile.kz on 3/29/15 5:08 PM.
 */
public interface EpayCallback {

    void process(Object o);
}
