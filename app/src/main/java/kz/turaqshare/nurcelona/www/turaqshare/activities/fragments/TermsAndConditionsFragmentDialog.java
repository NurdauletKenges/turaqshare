package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import butterknife.OnCheckedChanged;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseDialogFragment;

public class TermsAndConditionsFragmentDialog extends BaseDialogFragment {
    TermsListener mListener;

    public static TermsAndConditionsFragmentDialog newInstance() {
        return new TermsAndConditionsFragmentDialog();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogStyle);
        builder.setView(inflateDialogView(R.layout.fragment_dialog_terms_and_conditions, null));
        builder.setCancelable(false);
        return builder.create();
    }

    @OnCheckedChanged(R.id.checkbox)
    void OnCheckedChanged() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            mListener.onReadyClick();
            dismiss();
        }, 1500);

    }

    public void setListener(TermsListener listener) {
        this.mListener = listener;
    }

    public interface TermsListener {
        void onReadyClick();
    }
}
