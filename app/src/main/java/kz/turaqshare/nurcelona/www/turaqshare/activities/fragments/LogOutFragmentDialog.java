package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.google.firebase.auth.FirebaseAuth;

import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.LoginActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseDialogFragment;

public class LogOutFragmentDialog extends BaseDialogFragment {
    private FirebaseAuth mAuth;

    public static LogOutFragmentDialog newInstance() {
        return new LogOutFragmentDialog();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogStyle);
        builder.setView(inflateDialogView(R.layout.fragment_dialog_log_out, null));
        builder.setCancelable(true);
        builder.setPositiveButton("Выйти", (dialogInterface, i) -> {
            mAuth.signOut();
            if (getActivity() != null) {
                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finishAffinity();
            }
        });
        builder.setNegativeButton("Отмена", (dialogInterface, i) -> {
            dismiss();
        });
        return builder.create();
    }
}
