package kz.turaqshare.nurcelona.www.turaqshare.activities.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.PolyMaskTextChangedListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.LoginActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.SmsVerificationActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.TermsAndConditionsFragmentDialog;
import kz.turaqshare.nurcelona.www.turaqshare.activities.main.MainActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.User;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.SnackBarUtils;
import timber.log.Timber;

public class RegisterActivity extends BaseActivity {
    @BindView(R.id.etNumber)
    MaskedEditText etNumber;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etCar)
    EditText etCar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.linearLayout2)
    LinearLayout linearLayout2;
    @BindView(R.id.mainLayout)
    ConstraintLayout mainLayout;
    @BindView(R.id.etEmail)
    EditText etMail;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();
        showLoading(false);

        final List<String> affineFormats = new ArrayList<>();
        affineFormats.add("[A]-[000]-[AAA]");

        final MaskedTextChangedListener listener = new PolyMaskTextChangedListener(
                "KZ-[000]-[AAA]-[00]",
                affineFormats,
                etCar,
                (maskFilled, extractedValue) -> {
                    //Log.e(RegisterActivity.class.getSimpleName(), extractedValue);
                    //Log.e(RegisterActivity.class.getSimpleName(), String.valueOf(maskFilled));
                    //Log.e(RegisterActivity.class.getSimpleName(), "ET: "+etCar.getText().toString()+":"+etCar.length());
                }
        );

        etCar.addTextChangedListener(listener);
        etCar.setOnFocusChangeListener(listener);
        //etCar.setHint(listener.placeholder());
    }

    @OnClick(R.id.sign_in)
    void onSignInClick(){
        startActivity(new Intent(this,LoginActivity.class));
    }

    private boolean isValid() {
        //Log.e(RegisterActivity.class.getSimpleName(), "ET: " + etCar.getText().toString() + ":" + etCar.length());
        if (etName.getText().toString().trim().isEmpty()) {
            etName.setError(getString(R.string.required_field));
            return false;
        }

        if (etNumber.getText().toString().trim().isEmpty()) {
            etNumber.setError(getString(R.string.required_field));
            return false;
        }
        if (etNumber.getText().toString().length() != 16) {
            etNumber.setError(getString(R.string.incorrect_value));
            return false;
        }

        if (etCar.getText().toString().trim().startsWith("KZ")) {
            if (etCar.getText().toString().trim().length() != 13) {
                etCar.setError(getString(R.string.incorrect_car_number));
                return false;
            }
        } else {
            if (etCar.getText().toString().trim().length() != 9) {
                etCar.setError(getString(R.string.incorrect_car_number));
                return false;
            }
        }

        if (etCar.getText().toString().trim().isEmpty()) {
            etCar.setError(getString(R.string.required_field));
            return false;
        }

        if (etMail.getText().toString().trim().isEmpty()) {
            etCar.setError(getString(R.string.required_field));
            return false;
        }

        return true;
    }

    @OnClick(R.id.register_button)
    void onRegisterClick() {
        if (!isValid())
            return;
        TermsAndConditionsFragmentDialog dialog = TermsAndConditionsFragmentDialog.newInstance();
        dialog.setListener(this::onReady);
        dialog.show(getSupportFragmentManager(), null);
    }

    private void onReady() {
        showLoading(true);
        //Log.e("RegisterActivity", "PhoneNumber" + etNumber.getText().toString());
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                etNumber.getText().toString(),        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        Timber.e("Verification Completed");
                        signInWithPhoneAuthCredential(phoneAuthCredential);
                        showLoading(false);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        Timber.e("Verification Failed");
                        showLoading(false);
                    }

                    @Override
                    public void onCodeSent(String verificationId,
                                           PhoneAuthProvider.ForceResendingToken token) {
                        // The SMS verification code has been sent to the provided phone number, we
                        // now need to ask the user to enter the code and then construct a credential
                        // by combining the code with a verification ID.

                        // Save verification ID and resending token so we can use them later
                        mVerificationId = verificationId;
                        mResendToken = token;
                        Intent i = new Intent(RegisterActivity.this, SmsVerificationActivity.class);
                        i.putExtra("verificationId", verificationId);
                        i.putExtra("name", etName.getText().toString());
                        i.putExtra("car", etCar.getText().toString());
                        i.putExtra("email", etMail.getText().toString());
                        startActivity(i);
                        showLoading(false);
                    }
                });
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    showLoading(false);
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseUser user = task.getResult().getUser();
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference("users").child(user.getUid());
                        User user1 = new User(etName.getText().toString(), user.getPhoneNumber(), etCar.getText().toString(), etMail.getText().toString());
                        myRef.setValue(user1);

                        startActivity(new Intent(this, MainActivity.class));

                    } else {
                        // Sign in failed, display a message and update the UI
                        //Log.w("SMSVerification", "signInWithCredential:failure", task.getException());
                        //Toast.makeText(SmsVerificationActivity.this, "Code is not right", Toast.LENGTH_SHORT).show();
                        SnackBarUtils.getSnackBar(mainLayout, getString(R.string.error_loading)).show();
                    }
                });
    }

    private void showLoading(boolean status) {
        linearLayout.setVisibility(status ? View.GONE : View.VISIBLE);
        linearLayout2.setVisibility(status ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(status ? View.VISIBLE : View.GONE);
    }
}
