package kz.turaqshare.nurcelona.www.turaqshare.activities.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import butterknife.BindView;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;

public class TermsActivity extends BaseActivity {
    @BindView(R.id.checkbox)
    CheckBox checkBox;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_dialog_terms_and_conditions);

        checkBox.setVisibility(View.GONE);
    }

}
