package kz.turaqshare.nurcelona.www.turaqshare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.chaos.view.PinView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.OnTextChanged;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.main.MainActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.User;
import kz.turaqshare.nurcelona.www.turaqshare.activities.registration.RegisterValuesActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.KeyboardUtils;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.SnackBarUtils;


public class SmsVerificationActivity extends BaseActivity {

    @BindView(R.id.code_input_view)
    PinView codeInputView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.mainLayout)
    ConstraintLayout mainLayout;

    private FirebaseAuth mAuth;
    private String verificationId;
    private String name;
    private String car;
    private String email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_verification);
        mAuth = FirebaseAuth.getInstance();
        Bundle extras = getIntent().getExtras();
        verificationId = extras.getString("verificationId", null);
        name = extras.getString("name", null);
        car = extras.getString("car", null);
        email = extras.getString("email", null);
    }

    @OnTextChanged(R.id.code_input_view)
    void onTextChanged() {
        if (codeInputView.getText().toString().trim().length() == 6) {
            KeyboardUtils.hideKeyboard(this);
            showLoading(true);
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, codeInputView.getText().toString().trim());
            signInWithPhoneAuthCredential(credential);
        }
    }

    private void showLoading(boolean status) {
        linearLayout.setVisibility(status ? View.GONE : View.VISIBLE);
        //linearLayout2.setVisibility(status ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        FirebaseUser user = task.getResult().getUser();
                        DatabaseReference myRef = database.getReference("users").child(user.getUid());
                        if (name != null && car != null) {
                            User user1 = new User(name, user.getPhoneNumber(), car, email);
                            myRef.setValue(user1);
                        }
                        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    startActivity(new Intent(SmsVerificationActivity.this, MainActivity.class));
                                } else {
                                    startActivity(new Intent(SmsVerificationActivity.this, RegisterValuesActivity.class));
                                }
                                //showLoading(false);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                showLoading(false);
                            }
                        });
                    } else {
                        // Sign in failed, display a message and update the UI
                        //Log.w("SMSVerification", "signInWithCredential:failure", task.getException());
                        //Toast.makeText(SmsVerificationActivity.this, "Code is not right", Toast.LENGTH_SHORT).show();
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                            SnackBarUtils.getSnackBar(mainLayout, getResources().getString(R.string.wrong_input_code)).show();
                        } else {
                            SnackBarUtils.getSnackBar(mainLayout, getString(R.string.error_loading)).show();
                        }
                        showLoading(false);
                    }
                });
    }
}
