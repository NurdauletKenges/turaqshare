package kz.turaqshare.nurcelona.www.turaqshare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.TutorialFragment;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.TutorialFragment2;
import kz.turaqshare.nurcelona.www.turaqshare.activities.main.MainActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.registration.RegisterActivity;

public class TutorialActivity extends BaseActivity {

    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.container)
    ViewPager mViewPager;
    @BindView(R.id.next_tv)
    TextView next_tv;


    private FirebaseAuth mAuth;

    /**
     * The {@link ViewPager} that will host the section contents.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_tutorial);
        mAuth = FirebaseAuth.getInstance();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        /*
      The {@link android.support.v4.view.PagerAdapter} that will provide
      fragments for each of the sections. We use a
      {@link FragmentPagerAdapter} derivative, which will keep every
      loaded fragment in memory. If this becomes too memory intensive, it
      may be best to switch to a
      {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        //Animation types
        //https://github.com/romandanylyk/PageIndicatorView
        pageIndicatorView.setAnimationType(AnimationType.WORM);

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0){
                    pageIndicatorView.setSelectedColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                    next_tv.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.colorAccent));
                }else {
                    pageIndicatorView.setSelectedColor(ContextCompat.getColor(TutorialActivity.this,R.color.white));
                    next_tv.setTextColor(ContextCompat.getColor(TutorialActivity.this,R.color.white));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @OnClick(R.id.next_tv)
    void onNextPageClick(){
        if (mViewPager.getCurrentItem() == 0)
            mViewPager.setCurrentItem(1,true);
        else {
            if (mAuth.getCurrentUser() != null)
                startActivity(new Intent(this, MainActivity.class));
            else
                startActivity(new Intent(this, RegisterActivity.class));

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position == 0){
                return new TutorialFragment();
            }else {
                return new TutorialFragment2();
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }
}
