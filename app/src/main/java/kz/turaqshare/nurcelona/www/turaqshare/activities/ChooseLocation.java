package kz.turaqshare.nurcelona.www.turaqshare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;

public class ChooseLocation extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    @BindView(R.id.bottom_sheet)
    LinearLayout llBottomSheet;
    private BottomSheetBehavior bottomSheetBehavior;
    private LatLng point;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_location);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        initMap();
        initBottomSheet();
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initBottomSheet() {
        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }




    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng astana = new LatLng(51.1605227, 71.4703558);
        //mMap.addMarker(new MarkerOptions().position(astana).title("Marker in Sydney"));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(astana, 11);
        mMap.moveCamera(cameraUpdate);

        mMap.setOnMapClickListener(point -> {
            this.point = point;
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(point).title("Your Parking position")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            bottomSheetBehavior.setHideable(true);
        });
    }

    @OnClick(R.id.bottom_sheet)
    void onFinishClick() {
        if (point != null) {
            Intent output = new Intent();
            output.putExtra("Latitude", point.latitude);
            output.putExtra("Longitude", point.longitude);
            setResult(RESULT_OK, output);
            finish();
        }
    }
}
