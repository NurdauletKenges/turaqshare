package kz.turaqshare.nurcelona.www.turaqshare.activities.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.PolyMaskTextChangedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.main.MainActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.User;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.SnackBarUtils;

public class RegisterValuesActivity extends BaseActivity {
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etCar)
    EditText etCar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.mainLayout)
    ConstraintLayout mainLayout;
    @BindView(R.id.etEmail)
    EditText etMail;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_value);
        mAuth = FirebaseAuth.getInstance();
        showLoading(false);

        final List<String> affineFormats = new ArrayList<>();
        affineFormats.add("[A]-[000]-[AAA]");

        final MaskedTextChangedListener listener = new PolyMaskTextChangedListener(
                "KZ-[000]-[AAA]-[00]",
                affineFormats,
                etCar,
                (maskFilled, extractedValue) -> {
                }
        );
        etCar.addTextChangedListener(listener);
        etCar.setOnFocusChangeListener(listener);
    }

    private boolean isValid() {
        //Log.e(RegisterActivity.class.getSimpleName(), "ET: " + etCar.getText().toString() + ":" + etCar.length());
        if (etName.getText().toString().trim().isEmpty()) {
            etName.setError(getString(R.string.required_field));
            return false;
        }
        if (etCar.getText().toString().trim().startsWith("KZ")) {
            if (etCar.getText().toString().trim().length() != 13) {
                etCar.setError(getString(R.string.incorrect_car_number));
                return false;
            }
        } else {
            if (etCar.getText().toString().trim().length() != 9) {
                etCar.setError(getString(R.string.incorrect_car_number));
                return false;
            }
        }
        if (etCar.getText().toString().trim().isEmpty()) {
            etCar.setError(getString(R.string.required_field));
            return false;
        }

        if (etMail.getText().toString().trim().isEmpty()) {
            etCar.setError(getString(R.string.required_field));
            return false;
        }

        return true;
    }

    @OnClick(R.id.next_button)
    void onNextClick() {
        if (!isValid())
            return;
        showLoading(true);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users").child(mAuth.getCurrentUser().getUid());
        String name = etName.getText().toString().trim();
        String car = etCar.getText().toString().trim();
        User user1 = new User(name, mAuth.getCurrentUser().getPhoneNumber(), car, etMail.getText().toString().trim());
        myRef.setValue(user1)
                .addOnSuccessListener(aVoid -> {
                    startActivity(new Intent(RegisterValuesActivity.this, MainActivity.class));
                })
                .addOnFailureListener(e -> {
                    SnackBarUtils.getSnackBar(mainLayout, getString(R.string.error_loading)).show();
                    showLoading(false);
                });
    }

    private void showLoading(boolean status) {
        linearLayout.setVisibility(status ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(status ? View.VISIBLE : View.GONE);
    }
}
