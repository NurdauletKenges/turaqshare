package kz.turaqshare.nurcelona.www.turaqshare.activities.main;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.Parking;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.SnackBarUtils;

public class RentActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.tv_start)
    TextView start_tv;
    @BindView(R.id.tv_start2)
    TextView start_tv_2;
    @BindView(R.id.tv_start3)
    TextView start_tv_3;
    @BindView(R.id.information)
    ImageView information;
    @BindView(R.id.text_view_1)
    TextView tv_start;
    @BindView(R.id.text_view_2)
    TextView tv_end;
    @BindView(R.id.tv_monday)
    TextView tv_monday;
    @BindView(R.id.tv_tuesday)
    TextView tv_tuesday;
    @BindView(R.id.tv_wednesday)
    TextView tv_wednesday;
    @BindView(R.id.tv_thursday)
    TextView tv_thursday;
    @BindView(R.id.tv_friday)
    TextView tv_friday;
    @BindView(R.id.tv_saturday)
    TextView tv_saturday;
    @BindView(R.id.tv_sunday)
    TextView tv_sunday;
    @BindView(R.id.constraintLayout)
    ConstraintLayout mainLayout;

    private Calendar current_time = Calendar.getInstance();
    private Calendar date_start = Calendar.getInstance();
    private Calendar date_end = Calendar.getInstance();
    private DateFormat df = new SimpleDateFormat("dd MMMM yyyy, HH:mm", new Locale("ru"));
    private ProgressDialog progressDialog;
    private Bundle mExtras;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_rent);

        tv_monday.setTag(0);
        tv_tuesday.setTag(0);
        tv_wednesday.setTag(0);
        tv_thursday.setTag(0);
        tv_friday.setTag(0);
        tv_saturday.setTag(0);
        tv_sunday.setTag(0);

        tv_monday.setOnClickListener(this);
        tv_tuesday.setOnClickListener(this);
        tv_wednesday.setOnClickListener(this);
        tv_thursday.setOnClickListener(this);
        tv_friday.setOnClickListener(this);
        tv_saturday.setOnClickListener(this);
        tv_sunday.setOnClickListener(this);

        start_tv.bringToFront();
        start_tv_2.bringToFront();
        start_tv_3.bringToFront();
        information.bringToFront();

        tv_start.setText(df.format(date_start.getTime()));
        tv_end.setText(df.format(date_end.getTime()));
        progressDialog = new ProgressDialog(this);

        mExtras = getIntent().getExtras();
    }

    @OnClick(R.id.text_layout)
    void onDateClicked() {
        showDateTimePicker(tv_start, date_start);
    }

    @OnClick(R.id.text_layout2)
    void onEndDateClicked() {
        showDateTimePicker(tv_end, date_end);
    }

    @OnClick(R.id.information)
    void onInformationClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Хотите ли Вы сдавать в этот день свое место каждую неделю?");
        builder.create().show();
    }

    @OnClick(R.id.confirm)
    void onConfirmClick() {
        if (valid()) {
            progressDialog.show();

            String number = mExtras.getString("number");
            String previous_number = mExtras.getString("previous_number", null);

            if (previous_number != null) {
                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("parkings")
                        .child(mExtras.getString("address"))
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child(previous_number);
                myRef.removeValue();
            }


            String frequency = tv_monday.getTag() + "," +
                    tv_tuesday.getTag() + "," + tv_wednesday.getTag() + ","
                    + tv_thursday.getTag() + "," + tv_friday.getTag() + ","
                    + tv_saturday.getTag() + "," + tv_sunday.getTag();

            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("parkings")
                    .child(mExtras.getString("address"))
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(number);
            Parking parking = new Parking(number,
                    mExtras.getString("address"), "onVerification",
                    51.130185, 71.436364, date_start.getTimeInMillis(),
                    date_end.getTimeInMillis(), frequency, null);
            myRef.setValue(parking).addOnSuccessListener(aVoid -> {
                progressDialog.dismiss();
                finish();
            }).addOnFailureListener(e -> {
                progressDialog.dismiss();
                SnackBarUtils.getSnackBar(mainLayout, getString(R.string.error_loading)).show();
            });
        }

    }

    private boolean valid() {
        long start = date_start.getTimeInMillis();
        long end = date_end.getTimeInMillis();
        long current = current_time.getTimeInMillis();
        if (start < current) {
            tv_start.requestFocus();
            tv_start.setError("Не может быть раньше чем сейчас");
            stopError();
            return false;
        }
        if (end <= start) {
            tv_end.requestFocus();
            tv_end.setError("Не может быть раньше чем \"начало\"");
            stopError();
            return false;
        }
        return true;
    }

    private void stopError() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            tv_start.setError(null);
            tv_end.setError(null);
        }, 2000);
    }

    public void showDateTimePicker(TextView tv, final Calendar date) {
        final Calendar currentDate = Calendar.getInstance();
        new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            date.set(year, monthOfYear, dayOfMonth);
            new TimePickerDialog(this, (view1, hourOfDay, minute) -> {
                date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                date.set(Calendar.MINUTE, minute);
                tv.setText(df.format(date.getTime()));
                tv.setError(null);
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), true).show();
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    @Override
    public void onClick(View view) {
        if (view.getTag().equals(1)) {
            view.setBackground(getResources().getDrawable(R.drawable.circle_drawable_white));
            ((TextView) view).setTextColor(Color.BLACK);
            view.setTag(0);
            return;
        }
        view.setBackground(getResources().getDrawable(R.drawable.circle_drawable));
        ((TextView) view).setTextColor(Color.WHITE);
        view.setTag(1);
    }
}
