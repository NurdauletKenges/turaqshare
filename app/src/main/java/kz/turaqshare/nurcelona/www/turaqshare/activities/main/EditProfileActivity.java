package kz.turaqshare.nurcelona.www.turaqshare.activities.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.SaveOrNotFragment;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.User;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.SnackBarUtils;

public class EditProfileActivity extends BaseActivity {
    @BindView(R.id.edit_name)
    EditText edit_name;
    @BindView(R.id.edit_email)
    EditText edit_email;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_view)
    CircleImageView imageView;
    @BindView(R.id.mainLayout)
    LinearLayout mainLayout;
    private Image image;
    private FirebaseDatabase mDatabase;
    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;
    private StorageReference storageReference;
    private DatabaseReference myRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        progressDialog = new ProgressDialog(this);
        storageReference = FirebaseStorage.getInstance().getReference();
        myRef = mDatabase.getReference("users").child(mAuth.getCurrentUser().getUid());


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        updateInfo();
        updateProfilePhoto();
    }

    private void updateInfo() {
        ValueEventListener infoListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    edit_name.setText(user.getName());
                    edit_email.setText(user.getEmail());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                // ...
                SnackBarUtils.getSnackBar(mainLayout, getString(R.string.error_loading)).show();
            }

        };
        myRef.addListenerForSingleValueEvent(infoListener);
    }

    private void uploadFile(Uri filePath) {
        if (filePath != null) {
            progressDialog.setTitle("Загружается");
            progressDialog.show();

            StorageReference ref = storageReference.child("profileImages")
                    .child(mAuth.getCurrentUser().getUid())
                    .child("profile");

            ref.putFile(filePath)
                    .addOnSuccessListener(taskSnapshot -> {
                        progressDialog.dismiss();
                        finish();
                        //Toast.makeText(ParkingInfoActivity.this, "Файл", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(e -> {
                        progressDialog.dismiss();
                        //Toast.makeText(ParkingInfoActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    })
                    .addOnProgressListener(taskSnapshot -> {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                .getTotalByteCount());
                        progressDialog.setMessage("Загружается " + (int) progress + "%");
                    });
        }
    }

    private void onSaveClick() {
        if (edit_name.getText().toString().trim().isEmpty()) {
            edit_name.setError("Поле не может быть пустым");
            return;
        }

        DatabaseReference myRef = mDatabase.getReference("users").child(mAuth.getCurrentUser().getUid()).child("name");
        DatabaseReference myRefEmail = mDatabase.getReference("users").child(mAuth.getCurrentUser().getUid()).child("email");
        if (image != null) {
            myRef.setValue(edit_name.getText().toString());
            myRefEmail.setValue(edit_email.getText().toString());
            Uri filePath = Uri.fromFile(new File(image.getPath()));
            uploadFile(filePath);
            return;
        }
        myRef.setValue(edit_name.getText().toString())
                .addOnSuccessListener(aVoid -> myRefEmail.setValue(edit_email.getText().toString())
                        .addOnSuccessListener(aVoid1 -> finish()));
    }

    @OnClick(R.id.add_photo)
    void onAddPhoto() {
        ImagePicker.create(this)
                .includeVideo(false)
                .single()
                .start();
    }

    private void updateProfilePhoto() {
        FirebaseStorage.getInstance()
                .getReference()
                .child("profileImages")
                .child(mAuth.getCurrentUser().getUid())
                .child("profile")
                .getDownloadUrl().addOnSuccessListener(uri ->
                Glide.with(this).load(uri)
                        .thumbnail(Glide.with(this).load(R.drawable.placeholder))
                        .into(imageView));
        //Glide.with(MainActivity.this).load(uri).into(imageView).onLoadStarted());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                onSaveClick();
                return true;
            case android.R.id.home:
                showDialogFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialogFragment() {
        SaveOrNotFragment fragment = SaveOrNotFragment.newInstance();
        fragment.setOnPositiveClickListener(new SaveOrNotFragment.MyListener() {
            @Override
            public void onPositiveClick() {
                onSaveClick();
            }

            @Override
            public void onNegativeClick() {
                finish();
            }
        });
        fragment.show(getSupportFragmentManager(), null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            image = ImagePicker.getFirstImageOrNull(data);
            if (image != null) {
                Bitmap bitmap = BitmapFactory.decodeFile(image.getPath());
                imageView.setImageBitmap(bitmap);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        showDialogFragment();
    }
}
