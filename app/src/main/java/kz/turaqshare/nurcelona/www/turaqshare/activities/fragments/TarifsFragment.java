package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseFragment;

public class TarifsFragment extends BaseFragment {
    public TarifsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tarifs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

}
