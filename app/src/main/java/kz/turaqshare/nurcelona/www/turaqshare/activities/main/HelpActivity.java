package kz.turaqshare.nurcelona.www.turaqshare.activities.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.SeeLocation;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;

public class HelpActivity extends BaseActivity implements OnMapReadyCallback {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        initMap();
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @OnClick(R.id.tv_phone)
    void onPhoneClick() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+7 (701) 51 61 194", null));
        startActivity(intent);
    }

    @OnClick(R.id.tv_mail)
    void onEmailClick() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@turaqshare.kz"});
        intent.putExtra(Intent.EXTRA_TEXT, "Опишите ситуацию, мы свяжемся с вами в ближайшее время");

        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    @OnClick(R.id.facebook)
    void onFacebookClick(){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/turaQshare/"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.telegram)
    void onTelegramClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/turaqshare_help"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.instagram)
    void onInstagramClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/turaqshare/"));
        startActivity(browserIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng astana = new LatLng(51.1605227, 71.4703558);
        LatLng NU = new LatLng(51.089942, 71.400665);
        googleMap.addMarker(new MarkerOptions().position(NU).title("Nazarbayev University"));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(astana, 10);
        googleMap.moveCamera(cameraUpdate);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.setOnMapClickListener(latLng -> {
            Intent intent = new Intent(this, SeeLocation.class);
            intent.putExtra("name", "Nazarbayev University");
            intent.putExtra("latitude", 51.089942);
            intent.putExtra("longitude", 71.400665);
            startActivity(intent);
        });
    }
}
