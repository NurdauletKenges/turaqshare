package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseFragment;

public class HistoryFragment extends BaseFragment {
    @BindView(R.id.tv_in_process)
    TextView process;
    @BindView(R.id.tv_archive)
    TextView archive;
    @BindView(R.id.tv_old)
    TextView old;

    public HistoryFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        onOldClick();
    }

    private void setDefault() {
        old.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.colorAccent));
        archive.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        process.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        old.setBackground(getResources().getDrawable(R.drawable.border));
        archive.setBackground(getResources().getDrawable(R.drawable.border));
        process.setBackground(getResources().getDrawable(R.drawable.border));

    }

    @OnClick(R.id.tv_old)
    void onOldClick() {
        setDefault();
        old.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.white));
        old.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
    }

    @OnClick(R.id.tv_archive)
    void onArchiveClick() {
        setDefault();
        archive.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.white));
        archive.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
    }

    @OnClick(R.id.tv_in_process)
    void onProcessClick() {
        setDefault();
        process.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.white));
        process.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
    }
}
