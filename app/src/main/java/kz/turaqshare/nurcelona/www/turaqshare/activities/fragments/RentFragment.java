package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseFragment;

@Deprecated
public class RentFragment extends BaseFragment {
    @BindView(R.id.tv_start)
    TextView start_tv;
    @BindView(R.id.tv_start2)
    TextView start_tv_2;
    @BindView(R.id.tv_start3)
    TextView start_tv_3;
    @BindView(R.id.information)
    ImageView information;
    @BindView(R.id.text_view_1)
    TextView tv_start;
    @BindView(R.id.text_view_2)
    TextView tv_end;
    private Calendar date_start = Calendar.getInstance();
    private Calendar date_end = Calendar.getInstance();
    private Calendar date_frequency = Calendar.getInstance();

    public RentFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rent, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        start_tv.bringToFront();
        start_tv_2.bringToFront();
        start_tv_3.bringToFront();
        information.bringToFront();
    }

    @OnClick(R.id.text_layout)
    void onDateClicked() {
        showDateTimePicker(tv_start, date_start);
    }

    @OnClick(R.id.text_layout2)
    void onEndDateClicked() {
        showDateTimePicker(tv_end, date_end);
    }


    public void showDateTimePicker(TextView tv, final Calendar date) {
        final Calendar currentDate = Calendar.getInstance();
        new DatePickerDialog(getContext(), (view, year, monthOfYear, dayOfMonth) -> {
            date.set(year, monthOfYear, dayOfMonth);
            new TimePickerDialog(getContext(), (view1, hourOfDay, minute) -> {
                date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                date.set(Calendar.MINUTE, minute);
                DateFormat df = new SimpleDateFormat("dd MMMM yyyy, HH:mm", new Locale("ru"));
                tv.setText(df.format(date.getTime()));
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), true).show();
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }
}
