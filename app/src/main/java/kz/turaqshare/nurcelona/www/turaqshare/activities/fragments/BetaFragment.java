package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.google.firebase.auth.FirebaseAuth;

import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseDialogFragment;

public class BetaFragment extends BaseDialogFragment {
    private FirebaseAuth mAuth;

    public static BetaFragment newInstance() {
        return new BetaFragment();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogStyle);
        builder.setView(inflateDialogView(R.layout.fragment_dialog_beta, null));
        builder.setCancelable(true);
        builder.setPositiveButton("ОК", (dialogInterface, i) -> {
        });
        return builder.create();
    }
}
