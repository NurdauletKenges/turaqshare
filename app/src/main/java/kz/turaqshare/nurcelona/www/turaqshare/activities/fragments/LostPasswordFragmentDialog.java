package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseDialogFragment;

public class LostPasswordFragmentDialog extends BaseDialogFragment {
    public static LostPasswordFragmentDialog newInstance() {
        return new LostPasswordFragmentDialog();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogStyle);
        builder.setView(inflateDialogView(R.layout.fragment_dialog_lost_password, null));
        builder.setCancelable(true);
        builder.setPositiveButton("Запросить", (dialogInterface, i) -> {
        });
        builder.setNegativeButton("Отмена", (dialogInterface, i) -> {
        });
        return builder.create();
    }
}
