package kz.turaqshare.nurcelona.www.turaqshare.activities.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.Parking;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.User;

public class HousingEstateActivity extends BaseActivity {

    @BindView(R.id.container)
    LinearLayout holder;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    private String title;

    private DateFormat df = new SimpleDateFormat("dd.MM", new Locale("ru"));
    private DateFormat tf = new SimpleDateFormat("HH:mm", new Locale("ru"));

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_housing_estate);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle mExtras = getIntent().getExtras();
        title = mExtras.getString("title");
        setTitle(title);


        mCollapsingToolbarLayout.setTitle(getTitle());
        mCollapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        mCollapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

        Glide.with(this).load(R.drawable.newworldjk).into(imageView);

        loadData();
    }

    private void loadData() {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference()
                .child("parkings")
                .child(title);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (holder != null) {
                    holder.removeAllViews();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String uuid = snapshot.getKey();
                        if (uuid != null) {
                            final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("users").child(uuid);
                            FirebaseStorage.getInstance()
                                    .getReference()
                                    .child("profileImages")
                                    .child(uuid)
                                    .child("profile")
                                    .getDownloadUrl().addOnSuccessListener(uri -> {

                                for (DataSnapshot inner_shot : snapshot.getChildren()) {
                                    Parking parking = inner_shot.getValue(Parking.class);
                                    if (parking != null) {
                                        CardView view = (CardView) getLayoutInflater().inflate(R.layout.item_zhk_layout, holder, false);
                                        ImageView imageView = view.findViewById(R.id.profile_image);
                                        TextView name = view.findViewById(R.id.name);
                                        TextView number = view.findViewById(R.id.number);
                                        TextView date = view.findViewById(R.id.date);
                                        TextView time = view.findViewById(R.id.time);
                                        if (parking.getStatus().equals("Taken")) {
                                            view.findViewById(R.id.taken).setVisibility(View.VISIBLE);
                                        }
                                        Calendar start = Calendar.getInstance();
                                        Calendar end = Calendar.getInstance();
                                        start.setTimeInMillis(parking.getStart());
                                        end.setTimeInMillis(parking.getEnd());
                                        date.setText(getString(R.string.date_format, df.format(start.getTime()), df.format(end.getTime())));
                                        time.setText(getString(R.string.date_format, tf.format(start.getTime()), tf.format(end.getTime())));

                                        number.setText(parking.getNumber());
                                        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                User user = dataSnapshot.getValue(User.class);
                                                if (user != null) {
                                                    name.setText(user.getName());
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                            }
                                        });


                                        Glide.with(HousingEstateActivity.this).load(uri).into(imageView);
                                        view.setOnClickListener(view1 -> {
                                            if (!parking.getStatus().equals("Taken")) {
                                                Intent intent = new Intent(HousingEstateActivity.this, BookActivity.class);
                                                intent.putExtra("title", name.getText() + " " + parking.getNumber());
                                                intent.putExtra("uuid", uuid);
                                                intent.putExtra("number", parking.getNumber());
                                                intent.putExtra("address", parking.getAddress());
                                                startActivity(intent);
                                            } else {
                                                Toast.makeText(HousingEstateActivity.this, "Парковка занята", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        holder.addView(view);
                                    }
                                }
                            });
                        }

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
