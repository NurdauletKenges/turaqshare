package kz.turaqshare.nurcelona.www.turaqshare.activities.registration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.main.RentActivity;

public class ChangeParkingInfoActivity extends BaseActivity {
    @BindView(R.id.etParkingNumber)
    EditText etParkingNumber;
    /*  @BindView(R.id.etAddress)
      EditText etAddress;*/
  /*  @BindView(R.id.etAddress)
    TextView etAddress;*/
    @BindView(R.id.holder_view)
    LinearLayout holder;
    @BindView(R.id.checkbox)
    CheckBox checkBox;
    @BindView(R.id.mainLayout)
    ConstraintLayout mainLayout;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.spinner_address)
    Spinner mSpinnerAddress;
    private LatLng point;
    //Firebase
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private int files_uploaded = 0;
    private ProgressDialog progressDialog;
    private String previous_number;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_info);
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        progressDialog = new ProgressDialog(this);
        title.setText("Измените ваше парковочное место");
        previous_number = getIntent().getExtras().getString("number");
        etParkingNumber.setText(previous_number);
    }

/*    @OnClick(R.id.etAddress)
    void onAddressClick() {
        BetaFragment.newInstance().show(getSupportFragmentManager(), null);
    }*/

   /* @OnClick(R.id.select_place_by_map)
    void onSelectClick() {
        startActivityForResult(new Intent(this, ChooseLocation.class), 70);
    }*/

    @OnClick(R.id.terms_and_conditions)
    void onTermsClick() {
        startActivity(new Intent(this, TermsActivity.class));
    }

    @OnClick(R.id.next_button)
    void onNextClick() {
        if (isValid()) {

            if (files_uploaded == 0) {
                Toast.makeText(this, "Загрузите хотя бы 1 документ", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!checkBox.isChecked()) {
                Toast.makeText(this, "Подтвердите правила и условия", Toast.LENGTH_SHORT).show();
                return;
            }

            progressDialog.setTitle("");
            progressDialog.setMessage("");
            progressDialog.show();


            Intent intent = new Intent(ChangeParkingInfoActivity.this, RentActivity.class);
            intent.putExtra("previous_number", previous_number);
            intent.putExtra("number", etParkingNumber.getText().toString());
            intent.putExtra("address", mSpinnerAddress.getSelectedItem().toString());
            startActivity(intent);
            progressDialog.dismiss();
            finish();
        }
        //startActivity(new Intent(ParkingInfoActivity.this, RentActivity.class));

    }

    @OnClick(R.id.choose_photo)
    void onChoosePhoto() {
        if (isValid()) {
            ImagePicker.create(this).start();
        }
    }

    @OnClick(R.id.choose_file)
    void onChooseFile() {
        if (isValid()) {
            //ImagePicker.create(this).start();
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            startActivityForResult(intent, 72);
        }
    }

    private boolean isValid() {
        if (etParkingNumber.getText().toString().trim().isEmpty()) {
            etParkingNumber.setError(getString(R.string.required_field));
            return false;
        }
/*        if (etAddress.getText().toString().trim().isEmpty()) {
            etAddress.setError(getString(R.string.required_field));
            return false;
        }*/

    /*    if (point == null) {
            Toast.makeText(this, "Укажите место на карте", Toast.LENGTH_SHORT).show();
            return false;
        }*/

        return true;
    }

    private void uploadFile(Uri filePath) {
        if (filePath != null) {
            String name = new File(filePath.toString()).getName();
            progressDialog.setTitle("Загружается " + name);
            progressDialog.show();
            progressDialog.setCancelable(false);

            StorageReference ref = storageReference.child("images")
                    .child(auth.getCurrentUser().getUid())
                    .child(mSpinnerAddress.getSelectedItem().toString())
                    .child(etParkingNumber.getText().toString())
                    .child(name);

            ref.putFile(filePath)
                    .addOnSuccessListener(taskSnapshot -> {
                        LinearLayout view = (LinearLayout) getLayoutInflater().inflate(R.layout.item_layout_file, holder, false);
                        ((TextView) view.findViewById(R.id.text)).setText(name + " загружен");
                        holder.addView(view);
                        progressDialog.dismiss();
                        files_uploaded++;
                        //Toast.makeText(ParkingInfoActivity.this, "Файл", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(e -> {
                        progressDialog.dismiss();
                        //Toast.makeText(ParkingInfoActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    })
                    .addOnProgressListener(taskSnapshot -> {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                .getTotalByteCount());
                        progressDialog.setMessage("Загружается " + (int) progress + "%");
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 70 && resultCode == RESULT_OK && data != null) {
            Double latitude = data.getDoubleExtra("Latitude", 0);
            Double longitude = data.getDoubleExtra("Longitude", 0);
            point = new LatLng(latitude, longitude);
        }

        if (requestCode == 72 && resultCode == RESULT_OK && data != null) {
            Uri filePath = data.getData();
            uploadFile(filePath);
        }

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);
            for (int i = 0; i < images.size(); i++) {
                Uri filePath = Uri.fromFile(new File(images.get(i).getPath()));
                uploadFile(filePath);
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
