package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseDialogFragment;

public class SaveOrNotFragment extends BaseDialogFragment {
    private MyListener listener;

    public static SaveOrNotFragment newInstance() {
        return new SaveOrNotFragment();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogStyle);
        builder.setView(inflateDialogView(R.layout.fragment_save_or_not, null));
        builder.setCancelable(true);
        builder.setPositiveButton("Сохранить", (dialogInterface, i) -> {
            if (listener != null) {
                listener.onPositiveClick();
            }
        });
        builder.setNegativeButton("Отмена", (dialogInterface, i) -> {
            if (listener != null) {
                listener.onNegativeClick();
            }
        });
        return builder.create();
    }

    public void setOnPositiveClickListener(MyListener listener) {
        this.listener = listener;
    }

    public interface MyListener {
        void onPositiveClick();

        void onNegativeClick();
    }


}
