package kz.turaqshare.nurcelona.www.turaqshare.activities.utils;

public enum EpayLanguage {
    RUSSIAN("rus"),
    KAZAKH("kaz"),
    ENGLISH("eng");

    private String lang;

    EpayLanguage(String lang) {
        this.lang = lang;
    }

    @Override
    public String toString() {
        return lang;
    }
}