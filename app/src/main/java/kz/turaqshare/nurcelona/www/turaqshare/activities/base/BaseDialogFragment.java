package kz.turaqshare.nurcelona.www.turaqshare.activities.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseDialogFragment extends DialogFragment {

    private Unbinder unbinder;

    protected View inflateDialogView(final int resource, final ViewGroup viewGroup) {
        final View inflate = LayoutInflater.from(getActivity()).inflate(resource, viewGroup);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    protected View inflateDialogView(LayoutInflater inflater, @LayoutRes int res, ViewGroup root) {
        View view = inflater.inflate(res, root, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    protected View inflateDialogView(View view) {
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

