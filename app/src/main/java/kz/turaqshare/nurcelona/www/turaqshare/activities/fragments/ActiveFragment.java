package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.adapters.ParkingSlotAdapter;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseFragment;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.Parking;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.SnackBarUtils;

public class ActiveFragment extends BaseFragment implements ParkingSlotAdapter.ParkingSlotListener {

    /* @BindView(R.id.holder_view)
     LinearLayout holder;*/
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.constraintLayout)
    ConstraintLayout mainLayout;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ParkingSlotAdapter mAdapter;
    private ArrayList<Parking> mDataSet = new ArrayList<>();
    private ValueEventListener postListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            mDataSet.clear();
            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                Parking parking = postSnapshot.getValue(Parking.class);
                if (parking != null) {
                    mDataSet.add(parking);
                }
            }
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public ActiveFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_active, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        initFireBase();
        initRecyclerView();
        updateData();
    }

    private void initRecyclerView() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new ParkingSlotAdapter(mDataSet, getContext());
        mAdapter.setListener(this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initFireBase() {
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null)
            mDatabase = FirebaseDatabase.getInstance().getReference()
                    .child("parkings")
                    .child("ЖК Новый Мир")
                    .child(mAuth.getCurrentUser().getUid());
    }

    private void updateData() {

        mDatabase.addValueEventListener(postListener);
    }


    @Override
    public void onDeleteClick(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Удалить парковку?");
        builder.setNegativeButton("Отмена", (dialogInterface, i) -> {

        });
        builder.setPositiveButton("Да", (dialogInterface, i) -> {
            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("parkings")
                    .child("ЖК Новый Мир")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(mDataSet.get(position).getNumber());
            myRef.removeValue().addOnSuccessListener(aVoid -> {
                updateData();
            }).addOnFailureListener(e -> SnackBarUtils.getSnackBar(mainLayout, getString(R.string.error_loading)));
        });
        builder.create().show();
    }
}
