package kz.turaqshare.nurcelona.www.turaqshare.activities.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseFragment;
import kz.turaqshare.nurcelona.www.turaqshare.activities.main.CreditActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.main.TransactionActivity;

public class PaymentFragment extends BaseFragment {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    public PaymentFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        progressBar.getProgressDrawable().setColorFilter(
                ContextCompat.getColor(getContext(), R.color.colorAccent), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    @OnClick(R.id.change_payment_option)
    void onPaymentChangeClick() {
        startActivity(new Intent(getContext(), CreditActivity.class));
    }

    @OnClick(R.id.show_button)
    void onShowClick() {
        startActivity(new Intent(getContext(), TransactionActivity.class));
    }
}
