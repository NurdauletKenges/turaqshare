package kz.turaqshare.nurcelona.www.turaqshare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import butterknife.BindView;
import butterknife.OnClick;
import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.base.BaseActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.fragments.LostPasswordFragmentDialog;
import kz.turaqshare.nurcelona.www.turaqshare.activities.main.MainActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.registration.RegisterActivity;
import kz.turaqshare.nurcelona.www.turaqshare.activities.utils.SnackBarUtils;

public class LoginActivity extends BaseActivity {
    @BindView(R.id.etLogin)
    MaskedEditText login;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.linearLayout2)
    LinearLayout linearLayout2;
    @BindView(R.id.mainLayout)
    ConstraintLayout mainLayout;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @OnClick(R.id.register)
    void onRegisterClick(){
        startActivity(new Intent(this,RegisterActivity.class));
    }

    @OnClick(R.id.lost_password)
    void onPasswordLost() {
        LostPasswordFragmentDialog.newInstance().show(getSupportFragmentManager(), null);
    }

    @OnClick(R.id.next_button)
    void onNextClick() {
        if (!isValid())
            return;
        showLoading(true);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                login.getText().toString(),           // Phone number to verify
                60,                                // Timeout duration
                TimeUnit.SECONDS,                    // Unit of timeout
                this,                        // Activity (for callback binding)
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        //Log.e("LoginActivity", "Verification Completed");
                        signInWithPhoneAuthCredential(phoneAuthCredential);
                        showLoading(false);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        SnackBarUtils.getSnackBar(mainLayout, "Слишком много попыток входа, попробуйте позже").show();
                        showLoading(false);
                    }

                    @Override
                    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                        // The SMS verification code has been sent to the provided phone number, we
                        // now need to ask the user to enter the code and then construct a credential
                        // by combining the code with a verification ID.

                        // Save verification ID and resending token so we can use them later
                        mVerificationId = verificationId;
                        Intent i = new Intent(LoginActivity.this, SmsVerificationActivity.class);
                        i.putExtra("verificationId", verificationId);
                        LoginActivity.this.startActivity(i);
                        showLoading(false);
                    }
                });
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    showLoading(false);
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        startActivity(new Intent(this, MainActivity.class));
                    } else {
                        // Sign in failed, display a message and update the UI
                        //Log.w("SMSVerification", "signInWithCredential:failure", task.getException());
                        //Toast.makeText(SmsVerificationActivity.this, "Code is not right", Toast.LENGTH_SHORT).show();
                        SnackBarUtils.getSnackBar(mainLayout, getString(R.string.error_loading)).show();
                    }
                });
    }

    private boolean isValid() {
        if (login.getText().toString().length() != 16) {
            login.setError(getString(R.string.incorrect_value));
            return false;
        }

        return true;
    }

    private void showLoading(boolean status) {
        linearLayout.setVisibility(status ? View.GONE : View.VISIBLE);
        linearLayout2.setVisibility(status ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(status ? View.VISIBLE : View.GONE);
    }
}
