package kz.turaqshare.nurcelona.www.turaqshare.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kz.turaqshare.nurcelona.www.turaqshare.R;
import kz.turaqshare.nurcelona.www.turaqshare.activities.objects.Parking;
import kz.turaqshare.nurcelona.www.turaqshare.activities.registration.ChangeParkingInfoActivity;

public class ParkingSlotAdapter extends RecyclerView.Adapter<ParkingSlotAdapter.MyViewHolder> {
    private ArrayList<Parking> mDataset;
    private Context mContext;
    private ParkingSlotListener mListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ParkingSlotAdapter(ArrayList<Parking> myDataset, Context mContext) {
        mDataset = myDataset;
        this.mContext = mContext;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ParkingSlotAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parking_slot, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.name.setText(mDataset.get(position).getAddress());
        holder.number.setText(mDataset.get(position).getNumber());
        holder.mainLayout.setOnClickListener(view -> {
            if (holder.change_icon.getVisibility() == View.GONE) {
                holder.change_icon.setVisibility(View.VISIBLE);
                holder.delete_icon.setVisibility(View.VISIBLE);
                holder.clock_icon.setVisibility(View.GONE);
            } else {
                holder.change_icon.setVisibility(View.GONE);
                holder.delete_icon.setVisibility(View.GONE);
                holder.clock_icon.setVisibility(View.VISIBLE);
            }
        });
        holder.change_icon.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, ChangeParkingInfoActivity.class);
            intent.putExtra("number", mDataset.get(position).getNumber());
            mContext.startActivity(intent);
        });

        holder.delete_icon.setOnClickListener(view -> mListener.onDeleteClick(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView number;
        public LinearLayout mainLayout;
        public ImageView change_icon;
        public ImageView delete_icon;
        public ImageView clock_icon;

        MyViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
            number = v.findViewById(R.id.number);
            mainLayout = v.findViewById(R.id.mainLayout);
            change_icon = v.findViewById(R.id.change_icon);
            delete_icon = v.findViewById(R.id.delete_icon);
            clock_icon = v.findViewById(R.id.clock_icon);
        }
    }

    public void setListener(ParkingSlotListener listener) {
        this.mListener = listener;
    }

    public interface ParkingSlotListener {
        void onDeleteClick(int position);
    }
}
